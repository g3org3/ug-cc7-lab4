import java.util.Random;

public class Sort {
	private int[] array;

	public Sort(int[] array){
		this.array = array;
	}

	public void quickSort(){
		quickSort(this.array, 0, this.array.length-1);
	}

	int partition(int arr[], int left, int right){
	      int i = left, j = right;
	      int tmp;
	      int pivot = arr[(left + right) / 2];
	     
	      while (i <= j) {
	            while (arr[i] < pivot)
	                  i++;
	            while (arr[j] > pivot)
	                  j--;
	            if (i <= j) {
	                  tmp = arr[i];
	                  arr[i] = arr[j];
	                  arr[j] = tmp;
	                  i++;
	                  j--;
	            }
	      };
	     
	      return i;
	}
 
	void quickSort(int arr[], int left, int right) {
	      int index = partition(arr, left, right);
	      if (left < index - 1)
	            quickSort(arr, left, index - 1);
	      if (index < right)
	            quickSort(arr, index, right);
	}

	public void fillRandom(){
		for (int i=0; i<array.length; i++) {
			array[i] = random(1, 1000000);
		}
	}

	public int random(int start, int end){
		Random rand = new Random();
		return rand.nextInt(end-start+1) + start;
	}

	public String toString(){
		String str = "";

		for (int i=0; i<array.length; i++) {
			System.out.print(array[i]+" ");
		}

		return str;
	}
}